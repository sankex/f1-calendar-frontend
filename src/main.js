import Vue from 'vue'
import App from './App.vue'
import VueMoment from 'vue-moment'
import moment from 'moment'
import router from './router'
import store from './store'
import './assets/css/main.scss'

Vue.config.productionTip = false
Vue.use(VueMoment, {
    moment
})

new Vue({
    router,
    store,
    data: {
        showTopMenu: false
    },
    render: h => h(App)
}).$mount('#app')
